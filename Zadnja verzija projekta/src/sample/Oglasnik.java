package sample;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Oglasnik {
    private ArrayList<Oglas> oglasi;
    private AlertMessage alert;

    public Oglasnik() {
        oglasi = new ArrayList<>();
        alert = new AlertMessage();
        procitaj();
    }

    //preuzima podatke iz baze
    public void procitaj() {
        oglasi.clear();
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            alert.alertDriver();
        }
        try (Connection connection = new Baza().getBaza()) {
            Statement st = connection.createStatement();
            String sql = ("SELECT * FROM oglasnik WHERE Deleted = 0");
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                int ID = rs.getInt("ID");
                String kategorija = rs.getString("Kategorija");
                String naslov = rs.getString("Naslov");
                double cijena = rs.getDouble("Cijena");
                String kratakOpis = rs.getString("Kratak_opis");
                int kontaktBroj = rs.getInt("Kontakt_broj");
                String email = rs.getString("email");
                String slika = rs.getString("Lokacija_slike");
                Oglas oglas = new Oglas(ID, kategorija, cijena, naslov, kratakOpis, slika, kontaktBroj, email);
                oglasi.add(oglas);
            }
            Function<String, String> toUpper = s -> s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
            oglasi.forEach(oglas -> oglas.setNaslov(toUpper.apply(oglas.getNaslov())));
            oglasi.forEach(oglas -> oglas.setKratakOpis(toUpper.apply(oglas.getKratakOpis())));

        } catch (SQLException e) {
            alert.alertBaza("procitaj funkcija.");
        }
    }

    public ArrayList<Oglas> getOglasi() {
        List<Oglas> list = oglasi.stream().collect(Collectors.toList());
        ArrayList<Oglas> result = new ArrayList<>();
        result.addAll(list);
        return result;
    }

    public void prikaz() {
        oglasi.forEach(oglas -> System.out.println(oglas.toString()));
    }
}
