package sample;

import javafx.scene.control.Alert;

public class AlertMessage implements AlertInterface{
    @Override
    public void alertBaza(String poruka) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Alert");
        alert.setHeaderText("Trenutno se nije moguće spojiti na bazu " + poruka);
        alert.showAndWait();
        System.exit(0);
    }

    @Override
    public void alertDriver() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Alert");
        alert.setHeaderText("Problem u driveru za bazu");
        alert.showAndWait();
        System.exit(0);
    }

    @Override
    public void alertSpremi() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Alert");
        alert.setHeaderText("Uspijesno ste unijeli oglas");
        alert.showAndWait();
    }

    @Override
    public void alertInitialize() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Alert");
        alert.setHeaderText("Problem u inicijalizaciji kontrolera");
        alert.showAndWait();
        System.exit(0);
    }

    @Override
    public void alertSlika() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Alert");
        alert.setHeaderText("Problem u spremanju slike");
        alert.showAndWait();
    }

    @Override
    public void alertStranica(String stranica){
        String tekst = "Problem u prikazu " + stranica + " stranice";
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Alert");
        alert.setHeaderText(tekst);
        alert.showAndWait();
    }

}
