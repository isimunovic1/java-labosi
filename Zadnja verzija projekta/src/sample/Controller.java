package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

public class Controller {
    @FXML
    private Button btnSlika = new Button();
    @FXML
    private TextField tbNaslov;
    @FXML
    private TextArea tbOpis;
    @FXML
    private TextField tbCijena;
    @FXML
    private TextField tbEmail;
    @FXML
    private TextField tbKontakt;
    @FXML
    private ChoiceBox cbKategorija;
    @FXML
    private VBox vbPrikaz = new VBox();
    @FXML
    private VBox vbAutomobili = new VBox();
    @FXML
    private VBox vbSport = new VBox();
    @FXML
    private VBox vbNekretnine = new VBox();
    @FXML
    private VBox vbGlazba = new VBox();
    @FXML
    private VBox vbMobiteli = new VBox();
    @FXML
    private VBox vbKucniLjubimci = new VBox();
    private Oglasnik oglasnik1;
    private AlertMessage alert;
    private String lokacijaSlike = "Nema sliku";

    @FXML
    public void initialize() {
        try {
            alert = new AlertMessage();
            oglasnik1 = new Oglasnik();
            oglasnik1.procitaj();
            prikazKategorija();
            btnSlika.setOnAction(event -> {
                spremiSliku();
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Inicijalizacija za prikaz pojedinih kategorija
    public void prikazKategorija() throws IOException {
        int brX = 0;
        int brY = 0;

        List<Oglas> automobili = oglasnik1.getOglasi().stream().filter(element -> element.getKategorija().equals("Automobili")).collect(Collectors.toList());
        List<Oglas> nekretnine = oglasnik1.getOglasi().stream().filter(element -> element.getKategorija().equals("Nekretnine")).collect(Collectors.toList());
        List<Oglas> sport = oglasnik1.getOglasi().stream().filter(element -> element.getKategorija().equals("Sport")).collect(Collectors.toList());
        List<Oglas> kucniLjubimci = oglasnik1.getOglasi().stream().filter(element -> element.getKategorija().equals("Ku?ni ljubimci")).collect(Collectors.toList());
        List<Oglas> mobiteli = oglasnik1.getOglasi().stream().filter(element -> element.getKategorija().equals("Mobiteli")).collect(Collectors.toList());
        List<Oglas> glazba = oglasnik1.getOglasi().stream().filter(element -> element.getKategorija().equals("Glazba")).collect(Collectors.toList());

        automobili.forEach(element -> {
            try {
                vbAutomobili.getChildren().add(gridKategorije(element));
                vbAutomobili.getStyleClass().add("background");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });

        nekretnine.forEach(element -> {
            try {
                vbNekretnine.getChildren().add(gridKategorije(element));
                vbNekretnine.getStyleClass().add("background");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });

        glazba.forEach(element -> {
            try {
                vbGlazba.getChildren().add(gridKategorije(element));
                vbGlazba.getStyleClass().add("background");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });

        kucniLjubimci.forEach(element -> {
            try {
                vbKucniLjubimci.getChildren().add(gridKategorije(element));
                vbKucniLjubimci.getStyleClass().add("background");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });

        sport.forEach(element -> {
            try {
                vbSport.getChildren().add(gridKategorije(element));
                vbSport.getStyleClass().add("background");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });

        mobiteli.forEach(element -> {
            try {
                vbMobiteli.getChildren().add(gridKategorije(element));
                vbMobiteli.getStyleClass().add("background");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });

        for (Oglas oglas : oglasnik1.getOglasi()) {
            GridPane grid = new GridPane();
            grid.getStyleClass().add("background");
            grid.setPadding(new Insets(10, 0, 10, 10));
            TextArea text = new TextArea();
            Button gumb1 = new Button("Save");
            gumb1.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
            gumb1.setMinSize(48, 30);
            Kordinate cordEdit = new Kordinate(brX, brY);
            brX++;
            gumb1.setOnMouseClicked(event -> {
                buttonClicked(cordEdit, text.getText());
            });
            Button gumb2 = new Button("Delete");
            gumb2.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
            gumb2.setMinSize(55, 30);
            Kordinate cordDelete = new Kordinate(brX, brY);
            brY++;
            gumb2.setOnMouseClicked(event -> {
                buttonClicked(cordDelete, text.getText());
            });
            text.setText(oglas.toString());
            text.setMinSize(50, 150);
            FileInputStream imageStream;
            try {
                imageStream = new FileInputStream("slike/" + oglas.getLokacijaSlike());
            } catch (Exception e) {
                imageStream = new FileInputStream("slike/noImageFound.jpg");
            }
            Image image = new Image(imageStream);
            ImageView imgView = new ImageView(image);
            imgView.setFitWidth(380);
            imgView.setFitHeight(250);
            grid.add(imgView, 0, 0);
            grid.add(text, 1, 0);
            grid.add(gumb1, 2, 0);
            grid.add(gumb2, 3, 0);
            vbPrikaz.getChildren().add(grid);
            brX = 0;

        }
    }

    //Stanica za home page
    public void homeButton(ActionEvent event) {
        try {
            Parent newRoot = FXMLLoader.load(getClass().getResource("home_page.fxml"));
            Scene scene = new Scene(newRoot, 675, 462);
            scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
            Stage stage = Main.getPrimaryStage();
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            alert.alertStranica("home");
        }
    }

    //Stranica za insert oglasa
    public void insertOglasButtonPushed(ActionEvent event) {
        try {
            Parent newRoot = FXMLLoader.load(getClass().getResource("insert.fxml"));
            Scene scene = new Scene(newRoot, 675, 462);
            scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
            Stage stage = Main.getPrimaryStage();
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            alert.alertStranica("insert");
        }
    }

    //Spremanje oglasa
    public void spremiOglas(ActionEvent event) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

        } catch (ClassNotFoundException e) {
            alert.alertDriver();
        }

        try (Connection connection = new Baza().getBaza()) {
            if (tbNaslov.getText() == null || tbNaslov.getText().trim().isEmpty()) {
                System.out.println("Polje naslov je obavezno");
                return;
            } else if (tbOpis.getText() == null || tbOpis.getText().trim().isEmpty()) {
                System.out.println("Polje kratki opis je obavezno");
                return;
            } else if (cbKategorija.getValue() == null) {
                System.out.println("Polje kategorija je obavezno");
                return;
            } else if (tbCijena.getText() == null || tbCijena.getText().trim().isEmpty()) {
                System.out.println("Polje cijena je obavezno");
                return;
            } else if (tbEmail.getText() == null || tbEmail.getText().trim().isEmpty()) {
                System.out.println("Polje email je obavezno");
                return;
            } else if (tbKontakt.getText() == null || tbKontakt.getText().trim().isEmpty()) {
                System.out.println("Polje kontakt je obavezno");
                return;
            } else {
                String query = "INSERT INTO oglasnik(Kategorija, Naslov, Cijena, Kratak_opis, Kontakt_broj, email, Lokacija_slike) VALUES(?, ?, ?, ?, ?, ?, ?)";
                PreparedStatement preparedStmt = connection.prepareStatement(query);
                preparedStmt.setString(1, cbKategorija.getValue().toString());
                preparedStmt.setString(2, tbNaslov.getText());
                preparedStmt.setDouble(3, doubleConvert(tbCijena.getText()));
                preparedStmt.setString(4, tbOpis.getText());
                preparedStmt.setInt(5, intConvert(tbKontakt.getText()));
                preparedStmt.setString(6, tbEmail.getText());
                preparedStmt.setString(7, lokacijaSlike);
                preparedStmt.executeUpdate();
                alert.alertSpremi();
            }
        } catch (SQLException e) {
            alert.alertBaza("spremiOglas funkcija");
        }

    }

    //Stranica za prikaz svih oglasa moguc edit i delete
    public void prikaz(ActionEvent events) {
        try {
            oglasnik1.procitaj();
            Parent newRoot = FXMLLoader.load(getClass().getResource("prikaz.fxml"));
            Scene scene = new Scene(newRoot, 675, 462);
            scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
            Stage stage = Main.getPrimaryStage();
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            alert.alertStranica("prikaz");
        }
    }

    //Pojedine kategorije
    public void prikazAutomobili(ActionEvent event) {
        try {
            oglasnik1.procitaj();
            Parent newRoot = FXMLLoader.load(getClass().getResource("prikazAutomobili.fxml"));
            Scene scene = new Scene(newRoot, 675, 462);
            scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
            Stage stage = Main.getPrimaryStage();
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            alert.alertStranica("automobili");
        }
    }

    public void prikazSport(ActionEvent event) {
        try {
            oglasnik1.procitaj();
            Parent newRoot = FXMLLoader.load(getClass().getResource("prikazSport.fxml"));
            Scene scene = new Scene(newRoot, 675, 462);
            scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
            Stage stage = Main.getPrimaryStage();
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            alert.alertStranica("sport");
        }
    }

    public void prikazNekretnine(ActionEvent event) {
        try {
            oglasnik1.procitaj();
            Parent newRoot = FXMLLoader.load(getClass().getResource("prikazNekretnine.fxml"));
            Scene scene = new Scene(newRoot, 675, 462);
            scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
            Stage stage = Main.getPrimaryStage();
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            alert.alertStranica("nekretnine");
        }
    }

    public void prikazGlazba(ActionEvent event) {
        try {
            oglasnik1.procitaj();
            Parent newRoot = FXMLLoader.load(getClass().getResource("prikazGlazba.fxml"));
            Scene scene = new Scene(newRoot, 675, 462);
            scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
            Stage stage = Main.getPrimaryStage();
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            alert.alertStranica("glazba");
        }
    }

    public void prikazLjubimci(ActionEvent event) {
        try {
            oglasnik1.procitaj();
            Parent newRoot = FXMLLoader.load(getClass().getResource("prikazKucniLjubimci.fxml"));
            Scene scene = new Scene(newRoot, 675, 462);
            scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
            Stage stage = Main.getPrimaryStage();
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            alert.alertStranica("kucni ljubimci");
        }
    }

    public void prikazMobiteli(ActionEvent event) {
        try {
            oglasnik1.procitaj();
            Parent newRoot = FXMLLoader.load(getClass().getResource("prikazMobiteli.fxml"));
            Scene scene = new Scene(newRoot, 675, 462);
            scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
            Stage stage = Main.getPrimaryStage();
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            alert.alertStranica("mobiteli");
        }
    }

    //Funkcija za update i delete oglasa
    @FXML
    private void buttonClicked(Kordinate cord, String podaci) {
        Oglas noviOglas = null;
        if (cord.getX() == 1) {
            Alert alrt = new Alert(Alert.AlertType.CONFIRMATION, "Želite li izbrisati oglas?", ButtonType.YES, ButtonType.NO);
            alrt.showAndWait();
            if (alrt.getResult() == ButtonType.YES) {
                try (Connection connection = new Baza().getBaza()) {
                    String query = "UPDATE oglasnik SET Deleted = 1 WHERE ID = ?";
                    PreparedStatement preparedStmt = connection.prepareStatement(query);
                    int br = 0;
                    for (Oglas oglas : oglasnik1.getOglasi()) {
                        if (br == cord.getY()) {
                            noviOglas = oglas;
                            break;
                        } else {
                            br++;
                        }
                    }
                    preparedStmt.setInt(1, noviOglas.getID());
                    preparedStmt.executeUpdate();
                } catch (SQLException e) {
                    alert.alertBaza("buttonClicked funkcija delete dio");
                }
                try {
                    oglasnik1.procitaj();
                    Parent newRoot = FXMLLoader.load(getClass().getResource("prikaz.fxml"));
                    Scene scene = new Scene(newRoot, 675, 462);
                    scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
                    Stage stage = Main.getPrimaryStage();
                    stage.setScene(scene);
                    stage.show();
                } catch (IOException e) {
                    alert.alertStranica("prikaz");
                }
            }
        } else if (cord.getX() == 0) {
            String[] parts = podaci.split("\n");
            String[] parts2 = null;
            HashMap<String, String> lista = new LinkedHashMap<>();
            for (String string : parts) {
                try {
                    parts2 = string.split(": ");
                    lista.put(parts2[0], parts2[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            String[] values = lista.values().toArray(new String[0]);
            int br = 0;
            for (Oglas oglas : oglasnik1.getOglasi()) {
                if (br == cord.getY()) {
                    noviOglas = oglas;
                    break;
                } else {
                    br++;
                }
            }
            try (Connection connection = new Baza().getBaza()) {
                String query = "UPDATE oglasnik SET Kategorija = ?, Naslov = ?, Cijena = ?, Kratak_opis = ?, Kontakt_broj = ?, email = ? WHERE ID = ?";
                PreparedStatement preparedStmt = connection.prepareStatement(query);
                preparedStmt.setString(1, values[0]);
                preparedStmt.setString(2, values[1]);
                preparedStmt.setDouble(3, doubleConvert(values[3]));
                preparedStmt.setString(4, values[2]);
                preparedStmt.setInt(5, intConvert(values[5]));
                preparedStmt.setString(6, values[4]);
                preparedStmt.setInt(7, noviOglas.getID());
                preparedStmt.executeUpdate();
                System.out.println("Updateano");
            } catch (SQLException e) {
                alert.alertBaza("buttonClicked funkcija update dio");
                e.printStackTrace();
            }
            try {
                oglasnik1.procitaj();
                Parent newRoot = FXMLLoader.load(getClass().getResource("prikaz.fxml"));
                Scene scene = new Scene(newRoot, 675, 462);
                scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
                Stage stage = Main.getPrimaryStage();
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
                alert.alertStranica("prikaz");
            }
        }
    }

    //Funkcija za spremanje slika
    public void spremiSliku() {
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(null);
        File outputfile = new File("slike/" + file.getName());
        if (file != null) {
            try {
                BufferedImage img = ImageIO.read(file);
                lokacijaSlike = file.getName();
                System.out.println(lokacijaSlike);
                ImageIO.write(img, "jpg", outputfile);
            } catch (Exception e) {
                alert.alertSlika();
            }
        } else {
            lokacijaSlike = "Nema slike";
        }
    }

    //dodaje oglas u grid za prikaz kategorija
    public GridPane gridKategorije(Oglas oglas) throws FileNotFoundException {
        GridPane grid = new GridPane();
        grid.getStyleClass().add("background");
        grid.setPadding(new Insets(10, 10, 10, 10));
        TextArea text = new TextArea();
        text.setText(oglas.toString());
        text.setMinSize(50, 150);
        text.setEditable(false);
        FileInputStream imageStream;
        try {
            imageStream = new FileInputStream("slike/" + oglas.getLokacijaSlike());
        } catch (Exception e) {
            imageStream = new FileInputStream("slike/noImageFound.jpg");
        }
        Image image = new Image(imageStream);
        ImageView imgView = new ImageView(image);
        imgView.setFitWidth(380);
        imgView.setFitHeight(250);
        grid.add(imgView, 0, 0);
        grid.add(text, 1, 0);
        return grid;
    }

    //Poziv stranica za prikaz kategorija
    @FXML
    protected void kategorijaAutomobili(ActionEvent e) {
        prikazAutomobili(e);
    }

    @FXML
    protected void kategorijaNekretnine(ActionEvent e) {
        prikazNekretnine(e);
    }

    @FXML
    protected void kategorijaSport(ActionEvent e) {
        prikazSport(e);
    }

    @FXML
    protected void kategorijaGlazba(ActionEvent e) {
        prikazGlazba(e);
    }

    @FXML
    protected void kategorijaKucniLjubimci(ActionEvent e) {
        prikazLjubimci(e);
    }

    @FXML
    protected void kategorijaMobiteli(ActionEvent e) {
        prikazMobiteli(e);
    }

    public <T> double doubleConvert(T broj) {
        return Double.parseDouble(broj.toString()) * 1.0;
    }

    public <T> int intConvert(T broj) {
        return Integer.parseInt(broj.toString());
    }
}
