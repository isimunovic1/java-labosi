package sample;

public interface AlertInterface {
    void alertBaza(String poruka);

    void alertDriver();

    void alertSpremi();

    void alertInitialize();

    void alertSlika();

    void alertStranica(String stranica);
}
