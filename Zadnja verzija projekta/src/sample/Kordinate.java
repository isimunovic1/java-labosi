package sample;

public class Kordinate {
    private int x;
    private int y;

    public Kordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
