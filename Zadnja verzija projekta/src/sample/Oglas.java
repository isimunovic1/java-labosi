package sample;

import java.io.Serializable;

public class Oglas implements Serializable {
    private int ID;
    private String kategorija;
    private double cijena;
    private String naslov;
    private String kratakOpis;
    private String lokacijaSlike;
    private int kontaktBroj;
    private String email;

    public Oglas(int ID, String kategorija, double cijena, String naslov, String kratakOpis, String lokacijaSlike, Integer kontaktBroj, String email) {
        this.ID = ID;
        this.kategorija = kategorija;
        this.cijena = cijena;
        this.naslov = naslov;
        this.kratakOpis = kratakOpis;
        this.lokacijaSlike = lokacijaSlike;
        this.kontaktBroj = kontaktBroj;
        this.email = email;
    }

    @Override
    public String toString() {
        return "kategorija: " + kategorija + '\n' +
                "naslov: " + naslov + '\n' +
                "Opis: " + kratakOpis + '\n' +
                "cijena u kunama: " + cijena + '\n' +
                "email: " + email + '\n' +
                "Kontakt broj: " + kontaktBroj + '\n';

    }

    public String getKategorija() {
        return kategorija;
    }

    public double getCijena() {
        return cijena;
    }

    public String getNaslov() {
        return naslov;
    }

    public String getKratakOpis() {
        return kratakOpis;
    }

    public String getLokacijaSlike() {
        return lokacijaSlike;
    }

    public int getKontaktBroj() {
        return kontaktBroj;
    }

    public String getEmail() {
        return email;
    }

    public int getID() {
        return ID;
    }

    public void setNaslov(String naslov) {
        this.naslov = naslov;
    }

    public void setKratakOpis(String kratakOpis) {
        this.kratakOpis = kratakOpis;
    }
}
