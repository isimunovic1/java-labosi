package sample;

import java.io.Serializable;

public class Oglas implements Serializable {
    private String kategorija;
    private double cijena;
    private String naslov;
    private String kratakOpis;
    private  String lokacijaSlike;
    private  int kontaktBroj;

    public Oglas(String kategorija,double cijena, String naslov, String kratakOpis, String lokacijaSlike, Integer kontaktBroj){
        this.kategorija=kategorija;
        this.cijena=cijena;
        this.naslov=naslov;
        this.kratakOpis=kratakOpis;
        this.lokacijaSlike=lokacijaSlike;
        this.kontaktBroj=kontaktBroj;
    }

    @Override
    public String toString() {
        return
                "kategorija='" + kategorija + '\'' +
                ", cijena=" + cijena +
                ", naslov='" + naslov + '\'' +
                ", kratakOpis='" + kratakOpis + '\'' +
                ", lokacijaSlike='" + lokacijaSlike + '\'' +
                ", kontaktBroj=" + kontaktBroj;
    }
}
