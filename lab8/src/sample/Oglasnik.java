package sample;

import java.io.*;
import java.util.ArrayList;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class Oglasnik implements Serializable{
    ArrayList<Oglas> oglasi = new ArrayList<>();

    public Oglasnik(){}
    public void dodajOglas (Oglas oglas){
        try {
            FileOutputStream file = new FileOutputStream("podaci.data",true);
            ObjectOutputStream writer = new ObjectOutputStream(file);
            writer.writeObject(oglas);
            writer.close();
            file.close();
        } catch (Exception ex) {
            System.err.println("failed to write ");
        }
    }

    public void procitaj() throws Exception{
        FileInputStream fis=new FileInputStream("podaci.data");

        while(true)
        {
            try{
                ObjectInputStream ois=new ObjectInputStream(fis);
                List <Oglas> xList;
                xList = Arrays.asList((Oglas)ois.readObject());
                //display its data
                for(Oglas oglas: xList){
                    oglasi.add(oglas);
                }
            }
            catch(EOFException e) {
                break;
            }
            catch(Exception e) {
                e.printStackTrace();
                break;
            }
        }
        fis.close();
    }
    public void prikaz(){
        for (Oglas oglas : oglasi){
            System.out.println(oglas.toString());
        }
    }
}
