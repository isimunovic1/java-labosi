package sample;

import javafx.application.Application;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));

        primaryStage.setTitle("Oglasnik");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
        Oglas oglas1 = new Oglas("automobili",1200.0,"Golf 4","Prodajem Golf 4 ispravan","golf.img",12345);
        Oglas oglas2 = new Oglas("automobili",12000.0,"Opel Astra","Prodajem registriran do 10 mjeseca","slika.img",12345);
        Oglasnik oglasnik1 = new Oglasnik();
        //oglasnik1.dodajOglas(oglas1);
        //oglasnik1.dodajOglas(oglas2);
        try {
            oglasnik1.procitaj();
            oglasnik1.prikaz();
        }
        catch(Exception e){
            System.out.println("Greska");
        }

    }
}
