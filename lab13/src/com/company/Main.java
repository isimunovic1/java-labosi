package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {

        //Zadatak 1

        List<String> dani = new ArrayList<>();
        dani.add("Ponedjeljak");
        dani.add("Utorak");
        dani.add("Srijeda");
        dani.add("Četvrtak");
        dani.add("Petak");
        dani.add("Subota");
        dani.add("Nedjelja");
        /*
        List<String> filter = dani.stream().filter(element -> element.endsWith("k")).collect(Collectors.toList());
        for (String dan:filter) {
            System.out.println(dan);
        }
         */

        //Zadatak 2
        /*
        List<String> filter = dani.stream().map(s -> s.substring(0, 3)).collect(Collectors.toList());
        for (String dan:filter) {
            System.out.println(dan);
        }
        */

        //Zadatak 3
        boolean postoji = dani.stream().anyMatch(element -> element.length()>8);
        if(postoji){
            System.out.println("Postoji element s vise od 8 znakova");
        }
        else{
            System.out.println("Nepostoji element s vise od 8 znakova");
        }
    }
}
