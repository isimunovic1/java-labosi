-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 14, 2020 at 04:30 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oglasnik`
--

-- --------------------------------------------------------

--
-- Table structure for table `oglasnik`
--

DROP TABLE IF EXISTS `oglasnik`;
CREATE TABLE IF NOT EXISTS `oglasnik` (
  `ID` int(15) NOT NULL AUTO_INCREMENT,
  `Kategorija` varchar(25) COLLATE utf8mb4_croatian_ci NOT NULL,
  `Naslov` varchar(25) COLLATE utf8mb4_croatian_ci NOT NULL,
  `Cijena` double NOT NULL,
  `Kratak_opis` varchar(50) COLLATE utf8mb4_croatian_ci NOT NULL,
  `Kontakt_broj` int(15) NOT NULL,
  `email` varchar(25) COLLATE utf8mb4_croatian_ci NOT NULL,
  `Lokacija_slike` varchar(50) COLLATE utf8mb4_croatian_ci NOT NULL,
  `Deleted` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci;

--
-- Dumping data for table `oglasnik`
--

INSERT INTO `oglasnik` (`ID`, `Kategorija`, `Naslov`, `Cijena`, `Kratak_opis`, `Kontakt_broj`, `email`, `Lokacija_slike`, `Deleted`) VALUES
(4, 'Automobili', 'Golf 4', 14000, 'Ispravan registriran ', 12354, 'test', 'golf4.jpg', 0),
(10, 'Nekretnine', 'Prodajem kucu', 420000, 'Spremna za useljenje \nsa 6 soba', 912941243, 'nekretnine@gmail.com', '02_montazne_potkrovlje_232346.jpg.jfif', 0),
(9, 'Sport', 'Bicikl Fuji', 4000, 'Ispravan s 32 brzine	', 12345, 'pero@gmail.com', 'bicikl-fuji-mtb-addy-275-1.1-17-crni-2019-791.jpg', 0),
(13, 'Glazba', 't', 12.33, 't', 123, 'te', 'akusticna_gitara.jpg', 1),
(14, 'Glazba', 'Gitara', 2500, 'Rabljena akusti?na', 987654321, 'marko123@gmail.com', 'akusticna_gitara.jpg', 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
