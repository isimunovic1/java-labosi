package com.company;

import java.util.HashMap;
import java.util.Map;

public class Poslovnica {
    private String naziv;
    private String lokacija;
    private Map<String,Integer> lijekovi=new HashMap<String,Integer>();
    private Map<String,Integer> materijali=new HashMap<String,Integer>();
    private Map<String,Integer> rukavice=new HashMap<String,Integer>();
    private Map<String,Integer> zaštitneMaske=new HashMap<String,Integer>();
    public Poslovnica(String naziv, String lokacija) {
        this.naziv = naziv;
        this.lokacija = lokacija;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getLokacija() {
        return lokacija;
    }

    public Map<String, Integer> getLijekovi() {
        return lijekovi;
    }

    public void setLijekovi(Map<String, Integer> lijekovi) {
        this.lijekovi = lijekovi;
    }

    public Map<String, Integer> getMaterijali() {
        return materijali;
    }

    public void setMaterijali(Map<String, Integer> materijali) {
        this.materijali = materijali;
    }

    public void setLokacija(String lokacija) {
        this.lokacija = lokacija;
    }

    public void dodajLijek(String lijek, int kolicina){
        lijekovi.put(lijek,kolicina);
    }
    public void dodajMaterijal(String materijal,int kolicina){
        materijali.put(materijal,kolicina);
    }

    public void dodajRukavice(String rukavice, int kolicina){
        this.rukavice.put(rukavice,kolicina);
    }
    public void dodajMaske(String maske,int kolicina){
        zaštitneMaske.put(maske,kolicina);
    }

    public void prikaz(){
        for (Map.Entry entry : lijekovi.entrySet()) {
            System.out.println(entry.getKey() + ", " + entry.getValue());
        }
        for (Map.Entry entry : materijali.entrySet()) {
            System.out.println(entry.getKey() + ", " + entry.getValue());
        }
        for (Map.Entry entry : rukavice.entrySet()) {
            System.out.println(entry.getKey() + ", " + entry.getValue());
        }
        for (Map.Entry entry : zaštitneMaske.entrySet()) {
            System.out.println(entry.getKey() + ", " + entry.getValue());
        }
        System.out.println("========================================================");
    }

}
