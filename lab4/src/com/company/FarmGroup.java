package com.company;

public class FarmGroup {
    private String naziv;
    private FarmCompany company1= new FarmCompany("Roche");
    private FarmCompany company2= new FarmCompany("Novartis");
    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public FarmCompany getCompany1() {
        return company1;
    }

    public FarmCompany getCompany2() {
        return company2;
    }

    public FarmGroup(String naziv) {
        this.naziv = naziv;
    }

    public void printComp1(){
        System.out.println(naziv+"->"+company1.getNaziv()+"->"+company1.getPoslovnica1().getNaziv()+"->"+company1.getPoslovnica1().getLokacija());
        getCompany1().getPoslovnica1().prikaz();
        System.out.println(naziv+"->"+company1.getNaziv()+"->"+company1.getPoslovnica2().getNaziv()+"->"+company1.getPoslovnica2().getLokacija());
        getCompany1().getPoslovnica2().prikaz();
        System.out.println(naziv+"->"+company1.getNaziv()+"->"+company1.getPoslovnica3().getNaziv()+"->"+company1.getPoslovnica3().getLokacija());
        getCompany1().getPoslovnica3().prikaz();
    }

    public void printComp2(){
        System.out.println(naziv+"->"+company2.getNaziv()+"->"+company2.getPoslovnica1().getNaziv()+"->"+company2.getPoslovnica1().getLokacija());
        getCompany2().getPoslovnica1().prikaz();
        System.out.println(naziv+"->"+company2.getNaziv()+"->"+company2.getPoslovnica2().getNaziv()+"->"+company2.getPoslovnica2().getLokacija());
        getCompany2().getPoslovnica2().prikaz();
    }
}
