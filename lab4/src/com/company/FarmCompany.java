package com.company;

public class FarmCompany {

    private String naziv;
    Poslovnica[] poslovnice = new Poslovnica[10];
    int brojac=0;
    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public Poslovnica getPoslovnica1() {
        return poslovnice[0];
    }

    public Poslovnica getPoslovnica2() {
        return poslovnice[1];
    }

    public Poslovnica getPoslovnica3() {
        return poslovnice[2];
    }

    public void dodajPoslovnicu(String naziv, String lokacija){
        poslovnice[brojac]=new Poslovnica(naziv,lokacija);
        brojac++;
    }
    public FarmCompany(String naziv) {
        this.naziv = naziv;
    }
}
