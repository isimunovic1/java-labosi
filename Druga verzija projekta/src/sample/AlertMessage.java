package sample;

import javafx.scene.control.Alert;

public class AlertMessage {

    public void alertBaza(String poruka){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Alert");
        alert.setHeaderText("Trenutno se nije moguće spojiti na bazu " + poruka);
        alert.showAndWait();
    }

    public void alertDriver(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Alert");
        alert.setHeaderText("Problem u driveru za bazu");
        alert.showAndWait();
    }

    public void alertHome(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Alert");
        alert.setHeaderText("Problem u prikazu home stranice");
        alert.showAndWait();
    }

    public void alertInsert(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Alert");
        alert.setHeaderText("Problem u prikazu insert stranice");
        alert.showAndWait();
    }

    public void alertPrikaz(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Alert");
        alert.setHeaderText("Problem u prikazu prikaz stranice");
        alert.showAndWait();
    }

    public void alertInitialize(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Alert");
        alert.setHeaderText("Problem u inicijalizaciji kontrolera");
        alert.showAndWait();
        System.exit(0);
    }

    public void alertSlika(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Alert");
        alert.setHeaderText("Problem u spremanju slike");
        alert.showAndWait();
    }
}
