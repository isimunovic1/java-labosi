package sample;
import java.sql.*;
import java.util.ArrayList;

public class Oglasnik{
    private ArrayList<Oglas> oglasi;
    private AlertMessage alert;

    public Oglasnik(){
        oglasi = new ArrayList<>();
        alert = new AlertMessage();
        procitaj();
    }

    public void procitaj(){
        oglasi.clear();
        try {
            Class.forName("com.mysql.jdbc.Driver");
        }
        catch (ClassNotFoundException e) {
            alert.alertDriver();
        }
        try (Connection connection = new Baza().getBaza()) {
            Statement st = connection.createStatement();
            String sql = ("SELECT * FROM oglasnik WHERE Deleted = 0");
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()) {
                int ID = rs.getInt("ID");
                String kategorija = rs.getString("Kategorija");
                String naslov = rs.getString("Naslov");
                double cijena = rs.getDouble("Cijena");
                String kratakOpis = rs.getString("Kratak_opis");
                int kontaktBroj = rs.getInt("Kontakt_broj");
                String email = rs.getString("email");
                String slika = rs.getString("Lokacija_slike");
                Oglas oglas = new Oglas(ID,kategorija,cijena,naslov,kratakOpis,slika,kontaktBroj,email);
                oglasi.add(oglas);
            }
        } catch (SQLException e) {
            alert.alertBaza("procitaj funkcija.");
        }
    }

    public ArrayList<Oglas> getOglasi() {
        return oglasi;
    }

    public void prikaz(){
        for (Oglas oglas : oglasi){
            System.out.println(oglas.toString());
        }
    }
}
