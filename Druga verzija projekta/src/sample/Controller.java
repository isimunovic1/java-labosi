package sample;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.*;

public class Controller{
    @FXML private Button btnSlika = new Button();
    @FXML private TextField tbNaslov;
    @FXML private TextArea tbOpis;
    @FXML private TextField tbCijena;
    @FXML private TextField tbEmail;
    @FXML private TextField tbKontakt;
    @FXML private ChoiceBox cbKategorija;
    @FXML private VBox vbPrikaz = new VBox();
    @FXML private VBox vbAutomobili = new VBox();
    @FXML private VBox vbSport = new VBox();
    @FXML private VBox vbNekretnine = new VBox();
    @FXML private VBox vbGlazba = new VBox();
    @FXML private VBox vbMobiteli = new VBox();
    @FXML private VBox vbKucniLjubimci = new VBox();
    private Oglasnik oglasnik1;
    private AlertMessage alert;
    private String lokacijaSlike = "Nema sliku";

    @FXML
    public void initialize(){
        try
        {
            alert = new AlertMessage();
            oglasnik1 = new Oglasnik();
            oglasnik1.procitaj();
            prikazKategorija("");
            prikazKategorija("kategorije");
            btnSlika.setOnAction(event -> { spremiSliku();});
        }
        catch(Exception e){
            alert.alertInitialize();
        }
    }

    //Inicijalizacija za prikaz pojedinih kategorija
    public void prikazKategorija(String kategorija) throws IOException {
        int brX=0;
        int brY=0;
        for (Oglas oglas:oglasnik1.getOglasi()) {
            if(!kategorija.equals("")){
                if(oglas.getKategorija().equals("Automobili")){
                    GridPane grid = gridKategorije(oglas);
                    vbAutomobili.getChildren().add(grid);
                }
                if(oglas.getKategorija().equals("Sport")){
                    GridPane grid = gridKategorije(oglas);
                    vbSport.getChildren().add(grid);
                }
                if(oglas.getKategorija().equals("Nekretnine")){
                    GridPane grid = gridKategorije(oglas);
                    vbNekretnine.getChildren().add(grid);
                }
                if(oglas.getKategorija().equals("Glazba")){
                    GridPane grid = gridKategorije(oglas);
                    vbGlazba.getChildren().add(grid);
                }
                if(oglas.getKategorija().equals("Mobiteli")){
                    GridPane grid = gridKategorije(oglas);
                    vbMobiteli.getChildren().add(grid);
                }
                if(oglas.getKategorija().equals("Kućni ljubimci")){
                    GridPane grid = gridKategorije(oglas);
                    vbKucniLjubimci.getChildren().add(grid);
                }
            }
            else {
                GridPane grid = new GridPane();
                grid.setPadding(new Insets(10, 10, 10, 10));
                TextArea text = new TextArea();
                Button gumb1 = new Button("Save");
                gumb1.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
                gumb1.setMinSize(48, 30);
                Kordinate cordEdit = new Kordinate(brX, brY);
                brX++;
                gumb1.setOnMouseClicked(event -> {
                    buttonClicked(cordEdit, text.getText());
                });
                Button gumb2 = new Button("Delete");
                gumb2.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
                gumb2.setMinSize(50, 30);
                Kordinate cordDelete = new Kordinate(brX, brY);
                brY++;
                gumb2.setOnMouseClicked(event -> {
                    buttonClicked(cordDelete, text.getText());
                });
                text.setText(oglas.toString());
                text.setMinSize(50, 150);
                FileInputStream imageStream;
                try {
                    imageStream = new FileInputStream("slike/" + oglas.getLokacijaSlike());
                } catch (Exception e) {
                    imageStream = new FileInputStream("slike/noImageFound.jpg");
                }
                Image image = new Image(imageStream);
                ImageView imgView = new ImageView(image);
                imgView.setFitWidth(380);
                imgView.setFitHeight(250);
                grid.add(imgView, 0, 0);
                grid.add(text, 1, 0);
                grid.add(gumb1, 2, 0);
                grid.add(gumb2, 3, 0);
                vbPrikaz.getChildren().add(grid);
                brX = 0;
            }
        }
    }

    //Stanica za home page
    public void homeButton(ActionEvent event){
        try {
            Parent newRoot = FXMLLoader.load(getClass().getResource("sample.fxml"));
            Scene scene = new Scene(newRoot, 675, 462);
            Stage stage = Main.getPrimaryStage();
            stage.setScene(scene);
            stage.show();
        }
        catch (IOException e){
            alert.alertHome();
        }
    }

    //Stranica za insert oglasa
    public void insertOglasButtonPushed(ActionEvent event){
        try {
            Parent newRoot = FXMLLoader.load(getClass().getResource("insert.fxml"));
            Scene scene = new Scene(newRoot, 675, 462);
            Stage stage = Main.getPrimaryStage();
            stage.setScene(scene);
            stage.show();
        }
        catch (IOException e){
            alert.alertInsert();
        }
    }

    //Spremanje oglasa
    public void spremiOglas(ActionEvent event){
        try {
            Class.forName("com.mysql.jdbc.Driver");

        } catch (ClassNotFoundException e) {
            alert.alertDriver();
        }
        try (Connection connection = new Baza().getBaza()) {
            String query = "INSERT INTO oglasnik(Kategorija, Naslov, Cijena, Kratak_opis, Kontakt_broj, email, Lokacija_slike) VALUES(?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            preparedStmt.setString(1, cbKategorija.getValue().toString());
            preparedStmt.setString(2, tbNaslov.getText());
            preparedStmt.setDouble(3, Double.parseDouble(tbCijena.getText())*1.0);
            preparedStmt.setString(4, tbOpis.getText());
            preparedStmt.setInt(5, Integer.parseInt(tbKontakt.getText()));
            preparedStmt.setString(6, tbEmail.getText());
            preparedStmt.setString(7, lokacijaSlike);
            preparedStmt.executeUpdate();
        } catch (SQLException e) {
            alert.alertBaza("spremiOglas funkcija");
        }
    }

    //Stranica za prikaz svih oglasa moguc edit i delete
    public void prikaz(ActionEvent events){
        try {
            oglasnik1.procitaj();
            Parent newRoot = FXMLLoader.load(getClass().getResource("prikaz.fxml"));
            Scene scene = new Scene(newRoot, 675, 462);
            Stage stage = Main.getPrimaryStage();
            stage.setScene(scene);
            stage.show();
        }
        catch (IOException e){
            alert.alertPrikaz();
        }
    }

    //Pojedine kategorije
    public void prikazAutomobili(ActionEvent event){
        try {
            oglasnik1.procitaj();
            Parent newRoot = FXMLLoader.load(getClass().getResource("prikazAutomobili.fxml"));
            Scene scene = new Scene(newRoot, 675, 462);
            Stage stage = Main.getPrimaryStage();
            stage.setScene(scene);
            stage.show();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void prikazSport(ActionEvent event){
        try {
            oglasnik1.procitaj();
            Parent newRoot = FXMLLoader.load(getClass().getResource("prikazSport.fxml"));
            Scene scene = new Scene(newRoot, 675, 462);
            Stage stage = Main.getPrimaryStage();
            stage.setScene(scene);
            stage.show();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void prikazNekretnine(ActionEvent event){
        try {
            oglasnik1.procitaj();
            Parent newRoot = FXMLLoader.load(getClass().getResource("prikazNekretnine.fxml"));
            Scene scene = new Scene(newRoot, 675, 462);
            Stage stage = Main.getPrimaryStage();
            stage.setScene(scene);
            stage.show();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void prikazGlazba(ActionEvent event){
        try {
            oglasnik1.procitaj();
            Parent newRoot = FXMLLoader.load(getClass().getResource("prikazGlazba.fxml"));
            Scene scene = new Scene(newRoot, 675, 462);
            Stage stage = Main.getPrimaryStage();
            stage.setScene(scene);
            stage.show();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void prikazLjubimci(ActionEvent event){
        try {
            oglasnik1.procitaj();
            Parent newRoot = FXMLLoader.load(getClass().getResource("prikazKucniLjubimci.fxml"));
            Scene scene = new Scene(newRoot, 675, 462);
            Stage stage = Main.getPrimaryStage();
            stage.setScene(scene);
            stage.show();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void prikazMobiteli(ActionEvent event){
        try {
            oglasnik1.procitaj();
            Parent newRoot = FXMLLoader.load(getClass().getResource("prikazMobiteli.fxml"));
            Scene scene = new Scene(newRoot, 675, 462);
            Stage stage = Main.getPrimaryStage();
            stage.setScene(scene);
            stage.show();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Funkcija za update i delete oglasa
    @FXML
    private void buttonClicked(Kordinate cord, String podaci){
        Oglas noviOglas=null;
        if(cord.getX()==1){
            Alert alrt = new Alert(Alert.AlertType.CONFIRMATION, "Želite li izbrisati oglas?", ButtonType.YES, ButtonType.NO);
            alrt.showAndWait();
            if (alrt.getResult() == ButtonType.YES) {
                try (Connection connection = new Baza().getBaza()) {
                    String query = "UPDATE oglasnik SET Deleted = 1 WHERE ID = ?";
                    PreparedStatement preparedStmt = connection.prepareStatement(query);
                    int br=0;
                    for (Oglas oglas:oglasnik1.getOglasi()) {
                        if(br==cord.getY()) {
                             noviOglas = oglas;
                             break;
                        }
                        else{
                            br++;
                        }
                    }
                    preparedStmt.setInt(1, noviOglas.getID());
                    preparedStmt.executeUpdate();
                } catch (SQLException e) {
                    alert.alertBaza("buttonClicked funkcija delete dio");
                }
                try {
                    oglasnik1.procitaj();
                    Parent newRoot = FXMLLoader.load(getClass().getResource("prikaz.fxml"));
                    Scene scene = new Scene(newRoot, 675, 462);
                    Stage stage = Main.getPrimaryStage();
                    stage.setScene(scene);
                    stage.show();
                }
                catch (IOException e){
                    alert.alertPrikaz();
                }
            }
        }
        else if(cord.getX()==0){
            String[] parts = podaci.split("\n");
            String[] parts2=null;
            HashMap<String, String> lista = new LinkedHashMap<>();
            for (String string:parts) {
                try {
                    parts2=string.split(": ");
                    lista.put(parts2[0],parts2[1]);
                }
                catch (Exception e){
                }
            }
            String[] values = lista.values().toArray(new String[0]);
            int br=0;
            for (Oglas oglas:oglasnik1.getOglasi()) {
                if(br==cord.getY()) {
                    noviOglas = oglas;
                    break;
                }
                else{
                    br++;
                }
            }
            try (Connection connection = new Baza().getBaza()) {
                String query = "UPDATE oglasnik SET Kategorija = ?, Naslov = ?, Cijena = ?, Kratak_opis = ?, Kontakt_broj = ?, email = ? WHERE ID = ?";
                PreparedStatement preparedStmt = connection.prepareStatement(query);
                preparedStmt.setString(1, values[0]);
                preparedStmt.setString(2, values[1]);
                preparedStmt.setDouble(3, Double.parseDouble(values[3]));
                preparedStmt.setString(4, values[2]);
                preparedStmt.setInt(5, Integer.parseInt(values[5]));
                preparedStmt.setString(6, values[4]);
                preparedStmt.setInt(7, noviOglas.getID());
                preparedStmt.executeUpdate();
                System.out.println("Updateano");
            } catch (SQLException e) {
                alert.alertBaza("buttonClicked funkcija update dio");
            }
            try {
                oglasnik1.procitaj();
                Parent newRoot = FXMLLoader.load(getClass().getResource("prikaz.fxml"));
                Scene scene = new Scene(newRoot, 675, 462);
                Stage stage = Main.getPrimaryStage();
                stage.setScene(scene);
                stage.show();
            }
            catch (IOException e){
                alert.alertPrikaz();
            }
        }
    }

    //Funkcija za spremanje slika
    public void spremiSliku(){
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(null);
        File outputfile = new File("slike/"+file.getName());
        if (file != null){
            try {
                BufferedImage img = ImageIO.read(file);
                lokacijaSlike = file.getName();
                System.out.println(lokacijaSlike);
                ImageIO.write(img, "jpg", outputfile);
            }
            catch (Exception e){
                alert.alertSlika();
            }
        }
        else{
            lokacijaSlike = "Nema slike";
        }
    }

    public GridPane gridKategorije(Oglas oglas) throws FileNotFoundException {
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        TextArea text = new TextArea();
        text.setText(oglas.toString());
        text.setMinSize(50, 150);
        text.setEditable(false);
        FileInputStream imageStream;
        try {
            imageStream = new FileInputStream("slike/" + oglas.getLokacijaSlike());
        } catch (Exception e) {
            imageStream = new FileInputStream("slike/noImageFound.jpg");
        }
        Image image = new Image(imageStream);
        ImageView imgView = new ImageView(image);
        imgView.setFitWidth(380);
        imgView.setFitHeight(250);
        grid.add(imgView, 0, 0);
        grid.add(text, 1, 0);
        return grid;
    }

    //Poziv stranica za prikaz kategorija
    @FXML
    protected void kategorijaAutomobili(ActionEvent e) throws IOException {
        prikazAutomobili(e);
    }
    @FXML
    protected void kategorijaNekretnine(ActionEvent e) throws IOException {
        prikazNekretnine(e);
    }
    @FXML
    protected void kategorijaSport(ActionEvent e) throws IOException {
        prikazSport(e);
    }
    @FXML
    protected void kategorijaGlazba(ActionEvent e) throws IOException {
        prikazGlazba(e);
    }
    @FXML
    protected void kategorijaKucniLjubimci(ActionEvent e) throws IOException {
        prikazLjubimci(e);
    }
    @FXML
    protected void kategorijaMobiteli(ActionEvent e) throws IOException {
        prikazMobiteli(e);
    }
}
