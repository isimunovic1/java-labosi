package sample;

import javafx.application.Application;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;


public class Main extends Application {
    private static Stage primaryStage;

    private void setPrimaryStage(Stage stage) {
        Main.primaryStage = stage;
    }

    static public Stage getPrimaryStage() {
        return Main.primaryStage;
    }
    @Override
    public void start(Stage primaryStage){
        try {
            setPrimaryStage(primaryStage);
            Parent root = FXMLLoader.load(getClass().getResource("/sample/sample.fxml"));
            Scene scene = new Scene(root, 675, 462);
            primaryStage.setTitle("Oglasnik");
            primaryStage.setScene(scene);
            primaryStage.show();
        }
        catch (Exception e){
            System.out.println("Nije moguće pokrenuti aplikaciju");
        }
    }


    public static void main(String[] args){
        launch(args);
    }
}
