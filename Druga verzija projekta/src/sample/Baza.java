package sample;

import javafx.scene.control.Alert;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Baza {

    private String url = "jdbc:mysql://localhost:3306/oglasnik";
    private String username = "oglasnik";
    private String password = "connect123xy";

    public Connection getBaza() throws SQLException {
        Connection connection = DriverManager.getConnection(url, username, password);
        return connection;
    }
}
