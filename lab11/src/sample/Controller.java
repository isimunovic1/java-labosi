package sample;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;

public class Controller{
    @FXML private TextField tfLab11 = new TextField();
    @FXML private Label lbLab11 = new Label();
    public void initialize(){
        tfLab11.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                lbLab11.setText(tfLab11.getText());
            }
        });

    }


}

