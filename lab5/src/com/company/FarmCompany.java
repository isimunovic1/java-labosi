package com.company;

import java.util.Map;

public class FarmCompany {

    private String naziv;
    Poslovnica[] poslovnice = new Poslovnica[10];
    int brojac=0;
    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public Poslovnica getPoslovnica1() {
        return poslovnice[0];
    }

    public Poslovnica getPoslovnica2() {
        return poslovnice[1];
    }

    public Poslovnica getPoslovnica3() {
        return poslovnice[2];
    }

    public void dodajPoslovnicu(String naziv, String lokacija){
        poslovnice[brojac]=new Poslovnica(naziv,lokacija);
        brojac++;
    }

    public void zatvoriPoslovnicu(){
        System.out.println("Praznim poslovnicu 1 u kompaniji 1");
        getPoslovnica1().prikaz();
        isprazniLijekove();
        isprazniMaterijale();
        System.out.println("Prikaz nakon pražnjenja");
        System.out.println("Poslovnica 1: ");
        getPoslovnica1().prikaz();
        System.out.println("Poslovnica 2:");
        getPoslovnica2().prikaz();
    }

    public void isprazniLijekove(){
        for(Map.Entry<String, Integer> entry : getPoslovnica1().getLijekovi().entrySet()){
            String k = entry.getKey();
            int v = entry.getValue();
            if(getPoslovnica2().getLijekovi().containsKey(k)){
                getPoslovnica2().getLijekovi().values().remove(k);
                getPoslovnica2().getLijekovi().put(k,getPoslovnica2().getLijekovi().get(k)+v);
            }
            else{
                getPoslovnica2().getLijekovi().put(k,v);
            }
        }
        getPoslovnica1().getLijekovi().clear();
    }

    public void isprazniMaterijale(){
        for(Map.Entry<String, Integer> entry : getPoslovnica1().getMaterijali().entrySet()){
            String k = entry.getKey();
            int v = entry.getValue();
            if(getPoslovnica2().getMaterijali().containsKey(k)){
                getPoslovnica2().getMaterijali().values().remove(k);
                getPoslovnica2().getLijekovi().put(k,getPoslovnica2().getMaterijali().get(k)+v);
            }
            else{
                getPoslovnica2().getMaterijali().put(k,v);
            }
        }
        getPoslovnica1().getMaterijali().clear();
    }
    public FarmCompany(String naziv) {
        this.naziv = naziv;
    }
}
