package com.company;

import java.util.Map;

public class Main {

    public static void main(String[] args) {
        var group = new FarmGroup("Bayer");
        group.getCompany1().dodajPoslovnicu("Poslovnica1","Bjelovar");
        group.getCompany1().dodajPoslovnicu("Poslovnica2","Zagreb");
        group.getCompany1().dodajPoslovnicu("Poslovnica3","Osijek");
        group.getCompany2().dodajPoslovnicu("PoslovnicaA","London");
        group.getCompany2().dodajPoslovnicu("PoslovnicaB","Paris");

        group.getCompany1().getPoslovnica1().dodajLijek("Voltaren",1);
        group.getCompany1().getPoslovnica1().dodajLijek("Lupocet",8);
        group.getCompany1().getPoslovnica1().dodajLijek("Ibuprofen",5);
        group.getCompany1().getPoslovnica1().dodajMaterijal("Voda",100);
        group.getCompany1().getPoslovnica1().dodajMaterijal("Prašak",70);

        group.getCompany1().getPoslovnica2().dodajLijek("Dermazin",13);
        group.getCompany1().getPoslovnica2().dodajLijek("Rojazol",18);
        group.getCompany1().getPoslovnica2().dodajLijek("Ibuprofen",5);
        group.getCompany1().getPoslovnica2().dodajMaterijal("Aloe vera",30);
        group.getCompany1().getPoslovnica2().dodajMaterijal("Prašak",70);
        group.getCompany1().getPoslovnica2().dodajMaterijal("Voda",77);

        group.getCompany1().getPoslovnica3().dodajLijek("Betrion",10);
        group.getCompany1().getPoslovnica3().dodajLijek("Maxitrol",14);
        group.getCompany1().getPoslovnica3().dodajLijek("Ibuprofen",5);
        group.getCompany1().getPoslovnica3().dodajMaterijal("Aloe vera",30);
        group.getCompany1().getPoslovnica3().dodajMaterijal("Prašak",70);
        group.getCompany1().getPoslovnica3().dodajMaterijal("Voda",77);

        group.printComp1();

        group.getCompany2().getPoslovnica1().dodajLijek("Betrion",10);
        group.getCompany2().getPoslovnica2().dodajLijek("Maxitrol",14);
        group.getCompany2().getPoslovnica1().dodajLijek("Ibuprofen",5);
        group.getCompany2().getPoslovnica2().dodajMaterijal("Aloe vera",30);
        group.getCompany2().getPoslovnica1().dodajMaterijal("Prašak",70);
        group.getCompany2().getPoslovnica2().dodajMaterijal("Voda",77);
        group.getCompany2().getPoslovnica1().dodajRukavice("Rukavice 30 bijele",13);
        group.getCompany2().getPoslovnica2().dodajRukavice("Rukavice 50 crne",13);
        group.getCompany2().getPoslovnica1().dodajMaske("Maske 30",15);
        group.getCompany2().getPoslovnica2().dodajMaske("Maske 50",8);

        group.printComp2();
        group.getCompany1().zatvoriPoslovnicu();
    }
}
