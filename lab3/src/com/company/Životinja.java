package com.company;

class Životinja {
    private bojaDlake bojaDlake;
    private String ime;

    public String getBojaDlake() {
        return bojaDlake.boja;
    }

    public String getIme() {
        return ime;
    }

    public Životinja(com.company.bojaDlake bojaDlake, String ime) {
        this.bojaDlake = bojaDlake;
        this.ime = ime;
    }
    void ispis(){
        System.out.println("Ovaj ispis je u klasi Životinja");
    }
}

class Pas extends Životinja{
    public Pas(com.company.bojaDlake bojaDlake,String ime) {
        super(bojaDlake,ime);
    }
    @Override
    void ispis(){
        System.out.println("Ovaj ispis je u klasi Pas");
    }
}

class bojaDlake{
    public String boja;

    public bojaDlake(String boja) {
        this.boja = boja;
    }
}

