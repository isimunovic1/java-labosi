package com.company;

public class Osoba {
    private String ime;

    public Osoba(String ime) {
        this.ime = ime;
    }

    public String getIme() {
        return ime;
    }
}
