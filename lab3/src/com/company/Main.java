package com.company;
public class Main {

    public static void main(String[] args) {
        var bojaDlakePas = new bojaDlake("smeđa");
        var bojaDlakeMacka = new bojaDlake("bijela");

        //klasa pas nasljeđuje klasu životinja
        //životinja ima composition sa klasom bojaDlake
        var Pas = new Pas(bojaDlakePas,"rex");
        System.out.println(Pas.getBojaDlake());

        var Osoba = new Osoba("Ivan");
        //association između osobe i životinje
        System.out.println(Osoba.getIme() + " je vlasnik životinje imena " + Pas.getIme());

        var Mačka = new Životinja(bojaDlakeMacka,"silvester");
       //override primjer
        Pas.ispis();
        Mačka.ispis();

        //overload primjer
        var overload=new Overload();
        System.out.println(overload.zbroj(1,2));
        System.out.println(overload.zbroj(1,2,3));

    }
}
