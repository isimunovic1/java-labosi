package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        //Zadatak 1
        /*
        ArrayList<String> imena = new ArrayList<>();
        imena.add("Ivan");
        imena.add("Filip");
        imena.add("Marko");
        imena.add("Karlo");
        imena.add("Patrik");
        System.out.println("Prije brisanja");
        for (String ime: imena) {
            System.out.println(ime);
        }
        imena.remove(2);
        System.out.println("Poslije brisanja");
        for (String ime: imena) {
            System.out.println(ime);
        }*/

        //Zadatak 2
        /*
        ArrayList<String> imena = new ArrayList<>();
        imena.add("Ivan");
        imena.add("Filip");
        imena.add("Marko");
        imena.add("Karlo");
        imena.add("Patrik");
        Collections.reverse(imena);
        for (String ime: imena) {
            System.out.println(ime);
        }*/

        //Zadatak 3
        /*
        HashSet<String> imena = new HashSet<>();
        imena.add("Ivan");
        imena.add("Filip");
        imena.add("Marko");
        imena.add("Karlo");
        imena.add("Patrik");
        System.out.println(imena.size());
        */

        //Zadatak 4
        /*
        HashSet<String> imena = new HashSet<>();
        imena.add("Ivan");
        imena.add("Filip");
        imena.add("Marko");
        imena.add("Karlo");
        imena.add("Patrik");
        System.out.println("Prije brisanja");
        for (String ime: imena) {
            System.out.println(ime);
        }
        imena.clear();
        System.out.println("Poslije brisanja");
        for (String ime: imena) {
            System.out.println(ime);
        }
        */

        //Zadatak 5
        /*
        TreeSet<Integer> brojevi1 = new TreeSet<>();
        TreeSet<Integer> brojevi2 = new TreeSet<>();
        brojevi1.add(1);
        brojevi1.add(2);
        brojevi1.add(3);
        brojevi2.add(4);
        brojevi2.add(5);
        brojevi2.add(6);
        for (Integer broj: brojevi1) {
            brojevi2.add(broj);
        }
        System.out.println("Prvi Treeset");
        for (Integer broj: brojevi1) {
            System.out.println(broj);
        }
        System.out.println("Drugi Treeset");
        for (Integer broj: brojevi2) {
            System.out.println(broj);
        }
        */

        //Zadatak 6
        /*
        HashMap<Integer, String> hash_map = new HashMap<Integer, String>();
        hash_map.put(1,"jedan");
        hash_map.put(2,"dva");
        hash_map.put(3,"tri");
        hash_map.put(5,"pet");
        hash_map.put(4,"cetiri");
        System.out.println(hash_map.entrySet());
         */

        //Zadatak 7
        HashMap<Integer, String> hash_map = new HashMap<Integer, String>();
        hash_map.put(1,"jedan");
        hash_map.put(2,"dva");
        hash_map.put(3,"tri");
        hash_map.put(5,"pet");
        hash_map.put(4,"cetiri");

        //Zadatak 8
        /*
        HashMap<Integer, String> hash_map = new HashMap<Integer, String>();
        hash_map.put(1,"jedan");
        hash_map.put(2,"dva");
        hash_map.put(3,"tri");
        hash_map.put(5,"pet");
        hash_map.put(4,"cetiri");
        System.out.println(hash_map.get(3));
         */

        //Zadatak 9
        /*
        HashMap<Integer, String> hash_map = new HashMap<Integer, String>();
        hash_map.put(1,"jedan");
        hash_map.put(2,"dva");
        hash_map.put(3,"tri");
        hash_map.put(5,"pet");
        hash_map.put(4,"cetiri");
        for (Map.Entry<Integer, String> entry : hash_map.entrySet()) {
            Object value = entry.getValue();
            System.out.println(value);
        }
         */

        //Zadatak 10
        /*
        TreeMap<Integer,String> brojevi = new TreeMap<>();
        brojevi.put(1,"jedan");
        brojevi.put(2,"dva");
        brojevi.put(3,"tri");
        brojevi.put(5,"pet");
        brojevi.put(4,"cetiri");
        System.out.println("Prije brisanja");
        System.out.println(brojevi);
        brojevi.clear();
        System.out.println("Poslije brisanja");
        System.out.println(brojevi);
         */
    }
}
